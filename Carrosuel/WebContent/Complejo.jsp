<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Cine Kinal</title>
<meta charset="utf-8">
 <link href="dist/css/bootstrap.min.css" rel="stylesheet">
<script src="assets/js/ie-emulation-modes-warning.js"></script>
<link href="carousel.css" rel="stylesheet">
<link type="text/css" rel="stylesheet" href="Complejo.css"/>
</head>
<body>
<div class="container">
	<div class="row clearfix">
		<div class="col-md-12 column">
			<nav class="navbar navbar-default navbar-fixed-top navbar-inverse" role="navigation">
				<div class="navbar-header">
					<ul class="nav navbar-nav">
						<li x;class="active">
							<a href="CineKinal.jsp" style="color: white; margin-left: 550px;margin-top: -20px;'"><h3>CINE KINAL</h3></a>
						</li>
					</ul>			
				</div>
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav">
						<li style="margin-left:1190px;margin-top:-80px;"x;class="active">
							<a href="Principal.jsp"><h4 style="color: white;">Ver Cartelera</h4></a>
						</li>
					</ul>			
				</div>
			</nav>
		</div>
	</div>
</div>
	<div>
	<iframe style="margin-top: 180px;margin-left: 80px;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d15442.52800390474!2d-90.5568399760866!3d14.620027400991052!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0000000000000000%3A0xb116b3a6b9053306!2sGaler%C3%ADas+Miraflores!5e0!3m2!1ses-419!2sgt!4v1424543624554" width="600" height="450" frameborder="0" style="border:0"></iframe>
	<div class="container">
	<div class="row clearfix">
		<div class="col-md-12 column">
			 <address style="margin-left: 615px;margin-top: -460px;"> <strong><h2>Cine Kinal, Miraflores</h2></strong><br /> 21 Avenida 4-32, Zona 11<br />Guatemala, Guatemla<br /> <abbr title="Phone">Tel�fono:</abbr> (502) 2208-0505</address>
			 <div class="container">
	<div class="row clearfix">
		<div class="col-md-12 column">
			<table class="table table-bordered" style="width: 75px;margin-left: 600px;height: -150px;">
				<thead>
					<tr>
						<th>
							Tipo
						</th>
						<th>
							Precio
						</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>
							3D
							(Adulto,Ni�o,Anciano,Joven)
						</td>
						<td>
							Q.70.00
						</td>
					</tr>
					<tr class="active">
						<td>
							2D
							(Adulto,Ni�o,Anciano,Joven)
						</td>
						<td>
							Q.35.00
						</td>
					</tr>
					<tr class="success">
						<td>
							Adulto,Joven
						</td>
						<td>
							Q.30.00
						</td>
					</tr>
					<tr class="warning">
						<td>
							Ni�o
							
						</td>
						<td>
							Q.25.00						
							</td>
						</tr>
					<tr class="danger">
						<td>
							Mi�rcoles y S�bados
						
						</td>
						<td>
							Q.35.00
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>
			<h2 class="salas">N�mero de salas</h2>
			 <p>10</p>
			 <div class="Primer">
			 </div>
		</div>
	</div>
</div>
	</div>
	<div>
	<iframe style="margin-top:150px;margin-left: 80px;" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3860.854692192135!2d-90.4857560172279!3d14.607352214098704!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8589a3011fbeec3d%3A0x10f35a823df9a7c2!2sCayala!5e0!3m2!1ses-419!2sgt!4v1424544573369" width="600" height="450" frameborder="0" style="border:0"></iframe>
	<address style="margin-left: 705px;margin-top: -450px;"> <strong><h2>Cine Kinal, Cayal�</h2></strong><br /> Bulevar Rafael Landivar 10-05 Paseo Cayal� Zona 16<br />Guatemala, Guatemla<br /> <abbr title="Phone">P:</abbr> (502)  2493-9600</address>
	<div class="row clearfix">
		<div class="col-md-12 column">
			<table class="table table-bordered" style="width: 75px;margin-left: 710px;height: -150px;">
				<thead>
					<tr>
						<th>
							Tipo
						</th>
						<th>
							Precio
						</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>
							3D
							(Adulto,Ni�o,Anciano,Joven)
						</td>
						<td>
							Q.75.00
						</td>
					</tr>
					<tr class="active">
						<td>
							2D
							(Adulto,Ni�o,Anciano,Joven)
						</td>
						<td>
							Q.40.00
						</td>
					</tr>
					<tr class="success">
						<td>
							Adulto,Joven
						</td>
						<td>
							Q.30.00
						</td>
					</tr>
					<tr class="warning">
						<td>
							Ni�o
							
						</td>
						<td>
							Q.25.00						
							</td>
						</tr>
					<tr class="danger">
						<td>
							Mi�rcoles y S�bados
						
						</td>
						<td>
							Q.35.00
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>
			<h2 class="salas2">N�mero de salas</h2>
			 <p class="parrafo">15</p>
			 <div class="Primer2">
			 </div>
		</div>
	</div>
</div>
	</div>
	</div>
		<div>
	<iframe style="margin-top:350px;margin-left: 80px;" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3856.5548512944965!2d-91.534481!3d14.850221000000001!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x858e981a3b974b4d%3A0x88741c4fae945aa3!2sCentro+Comercial+Pradera+Xela!5e0!3m2!1ses-419!2sgt!4v1424555107224" width="600" height="450" frameborder="0" style="border:0"></iframe>
	<address style="margin-left: 705px;margin-top: -450px;"> <strong><h2>Cine Kinal, Pradera Xela</h2></strong><br /> Avenida Las Am�ricas 7-12<br />Quetzaltenango, Guatemla<br /> <abbr title="Phone">P:</abbr> (502) 7767 7884</address>
	<div class="row clearfix">
		<div class="col-md-12 column">
			<table class="table table-bordered" style="width: 75px;margin-left: 710px;height: -150px;">
				<thead>
					<tr>
						<th>
							Tipo
						</th>
						<th>
							Precio
						</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>
							3D
							(Adulto,Ni�o,Anciano,Joven)
						</td>
						<td>
							Q.65.00
						</td>
					</tr>
					<tr class="active">
						<td>
							2D
							(Adulto,Ni�o,Anciano,Joven)
						</td>
						<td>
							Q.45.00
						</td>
					</tr>
					<tr class="success">
						<td>
							Adulto,Joven
						</td>
						<td>
							Q.25.00
						</td>
					</tr>
					<tr class="warning">
						<td>
							Ni�o
							
						</td>
						<td>
							Q.20.00						
							</td>
						</tr>
					<tr class="danger">
						<td>
							Mi�rcoles y S�bados
						
						</td>
						<td>
							Q.35.00
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>
			<h2 class="salas2">N�mero de salas</h2>
			 <p class="parrafo">8</p>
			 <div class="Primer2">
			 </div>
		</div>
	</div>
</div>
	</div>
	</div>
	<div>
	<iframe style="margin-top:350px;margin-left: 80px;" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3861.0058578552416!2d-90.507195!3d14.598742000000001!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8589a3c7ed24f801%3A0xd1288a488ff725e3!2sOakland+Mall!5e0!3m2!1ses-419!2sgt!4v1424555684935" width="600" height="450" frameborder="0" style="border:0"></iframe>
	<address style="margin-left: 705px;margin-top: -450px;"> <strong><h2>Cine Kinal, Oakland Mall</h2></strong><br />Diagonal 6, 13-01 zona 10<br />Guatemala, Guatemla<br /> <abbr title="Phone">P:</abbr> (502) 2208 0606</address>
	<div class="row clearfix">
		<div class="col-md-12 column">
			<table class="table table-bordered" style="width: 75px;margin-left: 710px;height: -150px;">
				<thead>
					<tr>
						<th>
							Tipo
						</th>
						<th>
							Precio
						</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>
							3D
							(Adulto,Ni�o,Anciano,Joven)
						</td>
						<td>
							Q.75.00
						</td>
					</tr>
					<tr class="active">
						<td>
							2D
							(Adulto,Ni�o,Anciano,Joven)
						</td>
						<td>
							Q.50.00
						</td>
					</tr>
					<tr class="success">
						<td>
							Adulto,Joven
						</td>
						<td>
							Q.30.00
						</td>
					</tr>
					<tr class="warning">
						<td>
							Ni�o
							
						</td>
						<td>
							Q.25.00						
							</td>
						</tr>
					<tr class="danger">
						<td>
							Mi�rcoles y S�bados
						
						</td>
						<td>
							Q.35.00
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>
			<h2 class="salas2">N�mero de salas</h2>
			 <p class="parrafo">10</p>
			 <div class="Primer2">
			 </div>
		</div>
	</div>
</div>
	</div>
	</div>
    <script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
</body>
</html>