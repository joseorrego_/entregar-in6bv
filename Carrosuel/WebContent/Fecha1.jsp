<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Cine Kinal</title>
<link href="dist/css/bootstrap.min.css" rel="stylesheet">
<script src="assets/js/ie-emulation-modes-warning.js"></script>
<link href="carousel.css" rel="stylesheet">
<link type="text/css" rel="stylesheet" href="Fecha1.css"/>
<meta charset="utf-8">
</head>
<body>
<div class="container">
	<div class="row clearfix">
		<div class="col-md-12 column">
			<nav class="navbar navbar-default navbar-fixed-top navbar-inverse" role="navigation">
				<div class="navbar-header">
					<h2 style="color: #FFFFFF;margin-left: 630px;">Cine Kinal</h2>
				</div>
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav">
						<li style="margin-left:-770px;"x;class="active">
							<a href="Principal.jsp"><h2 style="font-size: 14px;color: white;">Regresar a Cartelera</h2></a>
						</li>
					</ul>			
				</div>
			</nav>
		</div>
	</div>
</div>
<div class="container">
	<div class="row clearfix">
		<div class="col-md-12 column">
			 <a href="Complejo.jsp" class="btn btn-lg btn-default" type="button" style="margin-top: 100px;">Visita nuestros complejos</a>
		</div>
	</div>
</div>
<h3 class="complejo1">Cine Kinal - Miraflores</h3>
<div class="pelicula">
<a href="Elninodelapijamaenrayas.jsp">
<img src="Imagenes/Elnino.png"/></a>
<div class="container">
	<div class="row clearfix">
		<div class="col-md-12 column">
			 <a href="Elninodelapijamaenrayas.jsp" class="btn btn-sm" type="button" style="font-size:16px;margin-left:20px;margin-top:-210px;">El Ni�o Con el Pijama de Rayas</a>
			 <div class="tipo"><p style="margin-left: 3px;">2D Y 3D</p>></div>
		</div>
		<div class="container">
	<div class="row clearfix">
		<div class="col-md-12 column" style="margin-top: -85px;">
			<p style="width: 250px;">
				En el Berl�n de 1942, el peque�o Bruno es hijo de un comandante de un campo de concentraci�n.
			</p>
			<p>
				<a class="btn" href="Elninodelapijamaenrayas.jsp" style="margin-left: -15px;">Leer M�s �</a>
			</p>
		</div>
	</div>
</div>
	</div>
</div>
<div class="container" style="margin-left: 0px;margin-top: -190px;width: 290px;">
	<div class="row clearfix">
		<div class="col-md-12 column">
			<ul class="breadcrumb">
				<li>
					<a href="#" style="font-size:13px;">10:00 A.M</a> <span class="divider"></span>
				</li>
				<li>
					<a href="#" style="font-size:13px;">02:00 P.M</a> <span class="divider"></span>
				</li>
				<li class="active">
					<a href="#" style="font-size:13px;">08:30 P.M</a> <span class="divider"></span>
				</li>
			</ul>
		</div>
	</div>
</div>
</div>
<div class="pelicula">
<a href="Grandesheroes.jsp">
<img src="Imagenes/Grandeheroes.png"/></a>
<div class="container">
	<div class="row clearfix">
		<div class="col-md-12 column">
			 <a href="Grandesheroes.jsp" class="btn btn-sm" type="button" style="font-size:16px;margin-left:60px;margin-top:-210px;">Grandes Heroes</a>
			 <div class="tipo4"><p style="margin-left: 3px;">PARA TODA LA FAMILIA</p></div>
		</div>
<div class="container">
	<div class="row clearfix">
		<div class="col-md-12 column" style="margin-top: -85px;">
			<p style="width: 250px;">
				Es una comedia de aventuras cargada de accion sobre el prodigio de la robotica Hiro Hamada.
			</p>
			<p>
				<a class="btn" href="Grandesheroes.jsp" style="margin-left: -15px;">Leer M�s �</a>
			</p>
		</div>
	</div>
</div>
	</div>
</div>
<div class="container" style="margin-left: 0px;margin-top: -190px;width: 290px;">
	<div class="row clearfix">
		<div class="col-md-12 column">
			<ul class="breadcrumb">
				<li>
					<a href="#" style="font-size:13px;">10:00 A.M</a> <span class="divider"></span>
				</li>
				<li>
					<a href="#" style="font-size:13px;">02:00 P.M</a> <span class="divider"></span>
				</li>
				<li class="active">
					<a href="#" style="font-size:13px;">08:30 P.M</a> <span class="divider"></span>
				</li>
			</ul>
		</div>
	</div>
</div>
</div>
<h3 class="complejo2">Cine Kinal - Cayal�</h3>
<div class="pelicula">
<a href="Ironman.jsp">
<img src="Imagenes/Ironman3.png"/></a>
<div class="container">
	<div class="row clearfix">
		<div class="col-md-12 column">
			 <a href="Ironman.jsp" class="btn btn-sm" type="button" style="font-size:16px;margin-left:80px;margin-top:-210px;">Iron Man 3</a>
			 <div class="tipo"><p style="margin-left: 3px;">2D Y 3D</p>></div>
		</div>
				<div class="container">
	<div class="row clearfix">
		<div class="col-md-12 column" style="margin-top: -85px;">
			<p style="width: 250px;">
				En IRON MAN 3, de Marvel, el impertinente pero brillante industrial Tony Stark/Iron Man.
			</p>
			<p>
				<a class="btn" href="Ironman.jsp" style="margin-left: -15px;">Leer M�s �</a>
			</p>
		</div>
	</div>
</div>
	</div>
</div>
<div class="container" style="margin-left: 0px;margin-top: -190px;width: 290px;">
	<div class="row clearfix">
		<div class="col-md-12 column">
			<ul class="breadcrumb">
				<li>
					<a href="#" style="font-size:13px;">10:00 A.M</a> <span class="divider"></span>
				</li>
				<li>
					<a href="#" style="font-size:13px;">02:00 P.M</a> <span class="divider"></span>
				</li>
				<li class="active">
					<a href="#" style="font-size:13px;">08:30 P.M</a> <span class="divider"></span>
				</li>
			</ul>
		</div>
	</div>
</div>
</div>
<h3 class="complejo3">Cine Kinal - Pradera Xela, Quetzaltenango</h3>
<div class="pelicula">
<a href="Transformes.jsp">
<img src="Imagenes/Transformes.png"/></a>
<div class="container">
	<div class="row clearfix">
		<div class="col-md-12 column">
			 <a href="Transformes.jsp" class="btn btn-sm" type="button" style="font-size:16px;margin-left:80px;margin-top:-210px;">Transformes 3</a>
			 <div class="tipo"><p style="margin-left: 3px;">2D Y 3D</p>></div>
		</div>
						<div class="container">
	<div class="row clearfix">
		<div class="col-md-12 column" style="margin-top: -85px;">
			<p style="width: 250px;">
				Transformers: la era de la extinci�n' nos sit�a cuatro a�os despu�s del incidente de Chicago.
			</p>
			<p>
				<a class="btn" href="Transformes.jsp" style="margin-left: -15px;">Leer M�s �</a>
			</p>
		</div>
	</div>
</div>
	</div>
</div>
<div class="container" style="margin-left: 0px;margin-top: -190px;width: 290px;">
	<div class="row clearfix">
		<div class="col-md-12 column">
			<ul class="breadcrumb">
				<li>
					<a href="#" style="font-size:13px;">10:00 A.M</a> <span class="divider"></span>
				</li>
				<li>
					<a href="#" style="font-size:13px;">02:00 P.M</a> <span class="divider"></span>
				</li>
				<li class="active">
					<a href="#" style="font-size:13px;">08:30 P.M</a> <span class="divider"></span>
				</li>
			</ul>
		</div>
	</div>
</div>
</div>
<div class="pelicula">
<a href="Primiciamortal.jsp">
<img src="Imagenes/Primiciamortal.png"/></a>
<div class="container">
	<div class="row clearfix">
		<div class="col-md-12 column">
			 <a href="Primiciamortal.jsp" class="btn btn-sm" type="button" style="font-size:16px;margin-left:80px;margin-top:-210px;">Primicia Mortal</a>
			 <div class="tipo8"><p style="margin-left: 3px;">ADULTO</p>></div>
		</div>
		<div class="container">
	<div class="row clearfix">
		<div class="col-md-12 column" style="margin-top: -85px;">
			<p style="width: 250px;">
				 Dan Gilroy ha escrito su opera prima, la historia sobre un ambicioso reportero freelance.
			</p>
			<p>
				<a class="btn" href="Primiciamortal.jsp" style="margin-left: -15px;">Leer M�s �</a>
			</p>
		</div>
	</div>
</div>
	</div>
</div>
<div class="container" style="margin-left: 0px;margin-top: -190px;width: 290px;">
	<div class="row clearfix">
		<div class="col-md-12 column">
			<ul class="breadcrumb">
				<li>
					<a href="#" style="font-size:13px;">10:00 A.M</a> <span class="divider"></span>
				</li>
				<li>
					<a href="#" style="font-size:13px;">02:00 P.M</a> <span class="divider"></span>
				</li>
				<li class="active">
					<a href="#" style="font-size:13px;">08:30 P.M</a> <span class="divider"></span>
				</li>
			</ul>
		</div>
	</div>
</div>
</div>
<h3 class="complejo4">Cine Kinal- Oakland Mall</h3>
<div class="pelicula">
<a href="Rapidoyfurioso.jsp">
<img src="Imagenes/Rapidoyfurioso.png"/></a>
<div class="container">
	<div class="row clearfix">
		<div class="col-md-12 column">
			 <a href="Rapidoyfurioso.jsp" class="btn btn-sm" type="button" style="font-size:16px;margin-left:60px;margin-top:-210px;">R�pido y Furioso 6</a>
			 <div class="tipo10"><p style="margin-left: 3px;">Adulto y Joven</p>></div>
		</div>
				<div class="container">
	<div class="row clearfix">
		<div class="col-md-12 column" style="margin-top: -125px;">
			<p style="width: 250px;">
				 Desde el robo de $100 millones de dolares en Rio que lograron hacer Dom (Vin Diesel) y Brian (Paul Walker), nuestros h�roes se han esparcido por todo el globo.
			</p>
			<p>
				<a class="btn" href="Rapidoyfurioso.jsp" style="margin-left: -15px;">Leer M�s �</a>
			</p>
		</div>
	</div>
</div>
	</div>
</div>
<div class="container" style="margin-left: 0px;margin-top: -190px;width: 290px;">
	<div class="row clearfix">
		<div class="col-md-12 column">
			<ul class="breadcrumb">
				<li>
					<a href="#" style="font-size:13px;">10:00 A.M</a> <span class="divider"></span>
				</li>
				<li>
					<a href="#" style="font-size:13px;">02:00 P.M</a> <span class="divider"></span>
				</li>
				<li class="active">
					<a href="#" style="font-size:13px;">08:30 P.M</a> <span class="divider"></span>
				</li>
			</ul>
		</div>
	</div>
</div>
</div>
</body>
</html>