<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Cine Kinal</title>
<meta charset="utf-8">
 <link href="dist/css/bootstrap.min.css" rel="stylesheet">
<script src="assets/js/ie-emulation-modes-warning.js"></script>
<link href="carousel.css" rel="stylesheet">
<link type="text/css" rel="stylesheet" href="Principal.css"/>
</head>
<body>
<div class="container">
	<div class="row clearfix">
		<div class="col-md-12 column">
			<nav class="navbar navbar-default navbar-fixed-top navbar-inverse" role="navigation">
				<div class="navbar-header">
					<ul class="nav navbar-nav">
						<li x;class="active">
							<a href="CineKinal.jsp" style="color: white; margin-left: 550px;margin-top: -20px;'"><h3>CINE KINAL</h3></a>
						</li>
					</ul>			
				</div>
					<ul class="nav navbar-nav navbar-right">
						<li class="dropdown">
							 <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="margin-left: -180px;color: #FFFFFF;">Fechas de funci�n<strong class="caret"></strong></a>
							<ul class="dropdown-menu">
								<li>
									<a href="Fecha1.jsp">22 de Febrero del 2015</a>
								</li>
								<li class="divider">
								</li>
								<li>
									<a href="Fecha2.jsp">23 de Febrero del 2015</a>
								</li>
								<li class="divider">
								</li>
								<li>
									<a href="Fecha3.jsp">24 de Febrero del 2015</a>
								</li>
								<li class="divider">
								</li>
								<li>
									<a href="Fecha4.jsp">25 de Febrero del 2015</a>
								</li>
									<li class="divider">
								</li> 
							</ul>
						</li>
					</ul>
			</nav>
		</div>
	</div>
</div>
<div class="container">
	<div class="row clearfix">
		<div class="col-md-12 column">
			 <a href="Complejo.jsp" class="btn btn-lg btn-default" type="button" style="margin-top: 100px;">Visita nuestros complejos</a>
		</div>
	</div>
</div>
<h3 class="complejo1">Cine Kinal - Miraflores</h3>
<div class="pelicula">
<a href="LosJuegosDelHambre.jsp">
<img src="Imagenes/Thehungergames.png"/>
</a>
<div class="container">
	<div class="row clearfix">
		<div class="col-md-12 column">
			 <a href="LosJuegosDelHambre.jsp" class="btn btn-sm" type="button" style="font-size:16px;margin-left:40px;margin-top:-210px;">Los Juegos del Hambre</a>
			 <div class="tipo"><p style="margin-left: 3px;">2D Y 3D</p>></div>
			 
		</div>

		<div class="container">
	<div class="row clearfix">
		<div class="col-md-12 column" style="margin-top: -105px;">
			<p style="width: 250px;">
				El fen�meno mundial de Los juegos del hambre contin�a incendiando el universo con el estreno de Los juegos del hambre: Sinsajo Parte 1.
			</p>
			<p>
				<a class="btn" href="LosJuegosDelHambre.jsp" style="margin-left: -15px;">Leer M�s �</a>
			</p>
		</div>
	</div>
</div>
	</div>
</div>
	<div class="container" style="margin-left: 0px;margin-top: -190px;width: 290px;">
	<div class="row clearfix">
		<div class="col-md-12 column">
			<ul class="breadcrumb">
				<li>
					<a href="#" style="font-size:13px;">10:00 A.M</a> <span class="divider"></span>
				</li>
				<li>
					<a href="#" style="font-size:13px;">02:00 P.M</a> <span class="divider"></span>
				</li>
				<li class="active">
					<a href="#" style="font-size:13px;">08:30 P.M</a> <span class="divider"></span>
				</li>
			</ul>
		</div>
	</div>
</div>
</div>
<div class="pelicula">
<a href="Elninodelapijamaenrayas.jsp">
<img src="Imagenes/Elnino.png"/></a>
<div class="container">
	<div class="row clearfix">
		<div class="col-md-12 column">
			 <a href="Elninodelapijamaenrayas.jsp" class="btn btn-sm" type="button" style="font-size:16px;margin-left:20px;margin-top:-210px;">El Ni�o Con el Pijama de Rayas</a>
			 <div class="tipo"><p style="margin-left: 3px;">2D Y 3D</p>></div>
		</div>
		<div class="container">
	<div class="row clearfix">
		<div class="col-md-12 column" style="margin-top: -85px;">
			<p style="width: 250px;">
				En el Berl�n de 1942, el peque�o Bruno es hijo de un comandante de un campo de concentraci�n.
			</p>
			<p>
				<a class="btn" href="Elninodelapijamaenrayas.jsp" style="margin-left: -15px;">Leer M�s �</a>
			</p>
		</div>
	</div>
</div>
	</div>
</div>
<div class="container" style="margin-left: 0px;margin-top: -190px;width: 290px;">
	<div class="row clearfix">
		<div class="col-md-12 column">
			<ul class="breadcrumb">
				<li>
					<a href="#" style="font-size:13px;">10:00 A.M</a> <span class="divider"></span>
				</li>
				<li>
					<a href="#" style="font-size:13px;">02:00 P.M</a> <span class="divider"></span>
				</li>
				<li class="active">
					<a href="#" style="font-size:13px;">08:30 P.M</a> <span class="divider"></span>
				</li>
			</ul>
		</div>
	</div>
</div>
</div>
<div class="pelicula">
<a href="Grandesheroes.jsp">
<img src="Imagenes/Grandeheroes.png"/></a>
<div class="container">
	<div class="row clearfix">
		<div class="col-md-12 column">
			 <a href="Grandesheroes.jsp" class="btn btn-sm" type="button" style="font-size:16px;margin-left:60px;margin-top:-210px;">Grandes Heroes</a>
			 <div class="tipo4"><p style="margin-left: 3px;">PARA TODA LA FAMILIA</p></div>
		</div>
<div class="container">
	<div class="row clearfix">
		<div class="col-md-12 column" style="margin-top: -85px;">
			<p style="width: 250px;">
				Es una comedia de aventuras cargada de accion sobre el prodigio de la robotica Hiro Hamada.
			</p>
			<p>
				<a class="btn" href="Grandesheroes.jsp" style="margin-left: -15px;">Leer M�s �</a>
			</p>
		</div>
	</div>
</div>
	</div>
</div>
<div class="container" style="margin-left: 0px;margin-top: -190px;width: 290px;">
	<div class="row clearfix">
		<div class="col-md-12 column">
			<ul class="breadcrumb">
				<li>
					<a href="#" style="font-size:13px;">10:00 A.M</a> <span class="divider"></span>
				</li>
				<li>
					<a href="#" style="font-size:13px;">02:00 P.M</a> <span class="divider"></span>
				</li>
				<li class="active">
					<a href="#" style="font-size:13px;">08:30 P.M</a> <span class="divider"></span>
				</li>
			</ul>
		</div>
	</div>
</div>
</div>
<h3 class="complejo2">Cine Kinal - Cayal�</h3>
<div class="pelicula">
<img src="Imagenes/Harrypotter.png"/>
<div class="container">
	<div class="row clearfix">
		<div class="col-md-12 column">
			 <a href="Harrypotter.jsp" class="btn btn-sm" type="button" style="font-size:16px;margin-left:20px;margin-top:-210px;">Harry Potter (Reliquias de la muerte)</a>
			 <div class="tipo"><p style="margin-left: 3px;">2D Y 3D</p>></div>
		</div>
		<div class="container">
	<div class="row clearfix">
		<div class="col-md-12 column" style="margin-top: -105px;">
			<p style="width: 250px;">
				Al igual que en la primera parte de esta �ltima entrega cinematogr�fica inspirada en los libros superventas de la autora J.K. Rowling.
			</p>
			<p>
				<a class="btn" href="Harrypotter.jsp" style="margin-left: -15px;">Leer M�s �</a>
			</p>
		</div>
	</div>
</div>
	</div>
</div>
<div class="container" style="margin-left: 0px;margin-top: -190px;width: 290px;">
	<div class="row clearfix">
		<div class="col-md-12 column">
			<ul class="breadcrumb">
				<li>
					<a href="#" style="font-size:13px;">10:00 A.M</a> <span class="divider"></span>
				</li>
				<li>
					<a href="#" style="font-size:13px;">02:00 P.M</a> <span class="divider"></span>
				</li>
				<li class="active">
					<a href="#" style="font-size:13px;">08:30 P.M</a> <span class="divider"></span>
				</li>
			</ul>
		</div>
	</div>
</div>
</div>
<div class="pelicula">
<a href="Ironman.jsp">
<img src="Imagenes/Ironman3.png"/></a>
<div class="container">
	<div class="row clearfix">
		<div class="col-md-12 column">
			 <a href="Ironman.jsp" class="btn btn-sm" type="button" style="font-size:16px;margin-left:80px;margin-top:-210px;">Iron Man 3</a>
			 <div class="tipo"><p style="margin-left: 3px;">2D Y 3D</p>></div>
		</div>
				<div class="container">
	<div class="row clearfix">
		<div class="col-md-12 column" style="margin-top: -85px;">
			<p style="width: 250px;">
				En IRON MAN 3, de Marvel, el impertinente pero brillante industrial Tony Stark/Iron Man.
			</p>
			<p>
				<a class="btn" href="Ironman.jsp" style="margin-left: -15px;">Leer M�s �</a>
			</p>
		</div>
	</div>
</div>
	</div>
</div>
<div class="container" style="margin-left: 0px;margin-top: -190px;width: 290px;">
	<div class="row clearfix">
		<div class="col-md-12 column">
			<ul class="breadcrumb">
				<li>
					<a href="#" style="font-size:13px;">10:00 A.M</a> <span class="divider"></span>
				</li>
				<li>
					<a href="#" style="font-size:13px;">02:00 P.M</a> <span class="divider"></span>
				</li>
				<li class="active">
					<a href="#" style="font-size:13px;">08:30 P.M</a> <span class="divider"></span>
				</li>
			</ul>
		</div>
	</div>
</div>
</div>
<h3 class="complejo3">Cine Kinal - Pradera Xela, Quetzaltenango</h3>
<div class="pelicula">
<a href="Transformes.jsp">
<img src="Imagenes/Transformes.png"/></a>
<div class="container">
	<div class="row clearfix">
		<div class="col-md-12 column">
			 <a href="Transformes.jsp" class="btn btn-sm" type="button" style="font-size:16px;margin-left:80px;margin-top:-210px;">Transformes 3</a>
			 <div class="tipo"><p style="margin-left: 3px;">2D Y 3D</p>></div>
		</div>
						<div class="container">
	<div class="row clearfix">
		<div class="col-md-12 column" style="margin-top: -85px;">
			<p style="width: 250px;">
				Transformers: la era de la extinci�n' nos sit�a cuatro a�os despu�s del incidente de Chicago.
			</p>
			<p>
				<a class="btn" href="Transformes.jsp" style="margin-left: -15px;">Leer M�s �</a>
			</p>
		</div>
	</div>
</div>
	</div>
</div>
<div class="container" style="margin-left: 0px;margin-top: -190px;width: 290px;">
	<div class="row clearfix">
		<div class="col-md-12 column">
			<ul class="breadcrumb">
				<li>
					<a href="#" style="font-size:13px;">10:00 A.M</a> <span class="divider"></span>
				</li>
				<li>
					<a href="#" style="font-size:13px;">02:00 P.M</a> <span class="divider"></span>
				</li>
				<li class="active">
					<a href="#" style="font-size:13px;">08:30 P.M</a> <span class="divider"></span>
				</li>
			</ul>
		</div>
	</div>
</div>
</div>
<div class="pelicula">
<a href="Elhombrearana.jsp">
<img src="Imagenes/Elhombrearana.png"/></a>
<div class="container">
	<div class="row clearfix">
		<div class="col-md-12 column">
			 <a href="Elhombrearana.jsp" class="btn btn-sm" type="button" style="font-size:16px;margin-left:70px;margin-top:-210px;">El Hombre Ara�a 3</a>
			 <div class="tipo"><p style="margin-left: 3px;">2D Y 3D</p>></div>
		</div>
			<div class="container">
	<div class="row clearfix">
		<div class="col-md-12 column" style="margin-top: -120px;">
			<p style="width: 250px;">
				Es genial ser el Hombre Ara�a. Para Peter Parker, no hay nada que se le parezca a estar oscilando entre los rascacielos, ser el h�roe y pasar tiempo con Gwen Stacy.
			</p>
			<p>
				<a class="btn" href="Elhombrearana.jsp" style="margin-left: -15px;">Leer M�s �</a>
			</p>
		</div>
	</div>
</div>
	</div>
</div>
<div class="container" style="margin-left: 0px;margin-top: -190px;width: 290px;">
	<div class="row clearfix">
		<div class="col-md-12 column">
			<ul class="breadcrumb">
				<li>
					<a href="#" style="font-size:13px;">10:00 A.M</a> <span class="divider"></span>
				</li>
				<li>
					<a href="#" style="font-size:13px;">02:00 P.M</a> <span class="divider"></span>
				</li>
				<li class="active">
					<a href="#" style="font-size:13px;">08:30 P.M</a> <span class="divider"></span>
				</li>
			</ul>
		</div>
	</div>
</div>
</div>
<div class="pelicula">
<a href="Primiciamortal.jsp">
<img src="Imagenes/Primiciamortal.png"/></a>
<div class="container">
	<div class="row clearfix">
		<div class="col-md-12 column">
			 <a href="Primiciamortal.jsp" class="btn btn-sm" type="button" style="font-size:16px;margin-left:80px;margin-top:-210px;">Primicia Mortal</a>
			 <div class="tipo8"><p style="margin-left: 3px;">ADULTO</p>></div>
		</div>
		<div class="container">
	<div class="row clearfix">
		<div class="col-md-12 column" style="margin-top: -85px;">
			<p style="width: 250px;">
				 Dan Gilroy ha escrito su opera prima, la historia sobre un ambicioso reportero freelance.
			</p>
			<p>
				<a class="btn" href="Primiciamortal.jsp" style="margin-left: -15px;">Leer M�s �</a>
			</p>
		</div>
	</div>
</div>
	</div>
</div>
<div class="container" style="margin-left: 0px;margin-top: -190px;width: 290px;">
	<div class="row clearfix">
		<div class="col-md-12 column">
			<ul class="breadcrumb">
				<li>
					<a href="#" style="font-size:13px;">10:00 A.M</a> <span class="divider"></span>
				</li>
				<li>
					<a href="#" style="font-size:13px;">02:00 P.M</a> <span class="divider"></span>
				</li>
				<li class="active">
					<a href="#" style="font-size:13px;">08:30 P.M</a> <span class="divider"></span>
				</li>
			</ul>
		</div>
	</div>
</div>
</div>
<h3 class="complejo4">Cine Kinal- Oakland Mall</h3>
<div class="pelicula">
<a href="Rapidoyfurioso.jsp">
<img src="Imagenes/Rapidoyfurioso.png"/></a>
<div class="container">
	<div class="row clearfix">
		<div class="col-md-12 column">
			 <a href="Rapidoyfurioso.jsp" class="btn btn-sm" type="button" style="font-size:16px;margin-left:60px;margin-top:-210px;">R�pido y Furioso 6</a>
			 <div class="tipo10"><p style="margin-left: 3px;">Adulto y Joven</p>></div>
		</div>
				<div class="container">
	<div class="row clearfix">
		<div class="col-md-12 column" style="margin-top: -125px;">
			<p style="width: 250px;">
				 Desde el robo de $100 millones de dolares en Rio que lograron hacer Dom (Vin Diesel) y Brian (Paul Walker), nuestros h�roes se han esparcido por todo el globo.
			</p>
			<p>
				<a class="btn" href="Rapidoyfurioso.jsp" style="margin-left: -15px;">Leer M�s �</a>
			</p>
		</div>
	</div>
</div>
	</div>
</div>
<div class="container" style="margin-left: 0px;margin-top: -190px;width: 290px;">
	<div class="row clearfix">
		<div class="col-md-12 column">
			<ul class="breadcrumb">
				<li>
					<a href="#" style="font-size:13px;">10:00 A.M</a> <span class="divider"></span>
				</li>
				<li>
					<a href="#" style="font-size:13px;">02:00 P.M</a> <span class="divider"></span>
				</li>
				<li class="active">
					<a href="#" style="font-size:13px;">08:30 P.M</a> <span class="divider"></span>
				</li>
			</ul>
		</div>
	</div>
</div>
</div>
<div class="pelicula">
<a href="Cars.jsp">
<img src="Imagenes/Cars2.png"/></a>
<div class="container">
	<div class="row clearfix">
		<div class="col-md-12 column">
			 <a href="Cars.jsp" class="btn btn-sm" type="button" style="font-size:16px;margin-left:80px;margin-top:-210px;">Cars 2</a>
			 <div class="tipo"><p style="margin-left: 3px;">2D Y 3D</p>></div>
		</div>
						<div class="container">
	<div class="row clearfix">
		<div class="col-md-12 column" style="margin-top: -105px;">
			<p style="width: 250px;">
				 Rayo McQueen y su colega Mate participan en el primer Grand Prix Mundial, una emocionante carrera por etapas.
			</p>
			<p>
				<a class="btn" href="Cars.jsp" style="margin-left: -15px;">Leer M�s �</a>
			</p>
		</div>
	</div>
</div>
	</div>
</div>
<div class="container" style="margin-left: 0px;margin-top: -190px;width: 290px;">
	<div class="row clearfix">
		<div class="col-md-12 column">
			<ul class="breadcrumb">
				<li>
					<a href="#" style="font-size:13px;">10:00 A.M</a> <span class="divider"></span>
				</li>
				<li>
					<a href="#" style="font-size:13px;">02:00 P.M</a> <span class="divider"></span>
				</li>
				<li class="active">
					<a href="#" style="font-size:13px;">08:30 P.M</a> <span class="divider"></span>
				</li>
			</ul>
		</div>
	</div>
</div>
</div>
    <script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
</body>
</html>