<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta charset="utf-8">
<title>Cine Kinal</title>
 <link href="dist/css/bootstrap.min.css" rel="stylesheet">
<script src="assets/js/ie-emulation-modes-warning.js"></script>
<link href="carousel.css" rel="stylesheet">
</head>
<body>
<div class="container">
	<div class="row clearfix">
		<div class="col-md-12 column">
			<nav class="navbar navbar-default navbar-fixed-top navbar-inverse" role="navigation">
				<div class="navbar-header">
					<h2 style="color: #FFFFFF;margin-left: 630px;">Cine Kinal</h2>
				</div>
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav">
						<li style="margin-left:1190px;margin-top:-60px;"x;class="active">
							<a href="Principal.jsp">Ver Cartelera</a>
						</li>
					</ul>			
				</div>
			</nav>
		</div>
	</div>
</div>
<!-- Carousel
    ================================================== -->
    <br>
    <br>
    <br>
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
        <li data-target="#myCarousel" data-slide-to="3"></li>
      </ol>
      <div class="carousel-inner" role="listbox">
        <div class="item active">
          <img src="Imagenes/Enelbosque.png" alt="First slide">
          <div class="container">
            <div class="carousel-caption">
 
            </div>
          </div>
        </div>
        <div class="item">
          <img src="Imagenes/Lomejordemi.png" alt="Second slide">
          <div class="container">
            
          </div>
        </div>
        <div class="item">
          <img src="Imagenes/Elfrancotirador.png" alt="Second slide">
          <div class="container">
         
          </div>
        </div>
        <div class="item">
          <img src="Imagenes/Selma.png" alt="Third slide">
          <div class="container">
          </div>
        </div>
      </div>
       

    </div><!-- /.carousel -->
    <div class="container">
	<div class="row clearfix">
		<div class="col-md-12 column">
			<div class="jumbotron well">
				<h1>
					CINE KINAL
				</h1>
				<p>
					Este cine ofrece variedad de peliculas, en diferentes com�das salas en las cuales podr�s disfrutar de tu pel�cula favorita con la compa�ia adecuada,
					podr�s reservar tus entradas d�as antes de decidirte verla.
				</p>
				<p>
					<a class="btn btn-primary btn-large" href="Principal.jsp">Decidete por alguna pel�cula</a>
				</p>
			</div>
		</div>
	</div>
</div>
    <script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
</body>
</html>