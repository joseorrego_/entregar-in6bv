<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="dist/css/bootstrap.min.css" rel="stylesheet">
<script src="assets/js/ie-emulation-modes-warning.js"></script>
<link href="carousel.css" rel="stylesheet">
<link type="text/css" rel="stylesheet" href="Elninodelapijamaenrayas.css"/>
<meta charset="utf-8">
<title>CineKinal</title>
</head>
<body>
<div class="container">
	<div class="row clearfix">
		<div class="col-md-12 column">
			<nav class="navbar navbar-default navbar-fixed-top navbar-inverse" role="navigation">
				<div class="navbar-header">
					<h2 style="color: #FFFFFF;margin-left: 630px;">Cine Kinal</h2>
				</div>
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav">
						<li style="margin-left:-770px;"x;class="active">
							<a href="Principal.jsp"><h2 style="font-size: 14px;color: white;">Regresar a Cartelera</h2></a>
						</li>
					</ul>			
				</div>
			</nav>
		</div>
	</div>
</div>

<div class="container">
	<div class="row clearfix">
		<div class="col-md-12 column">
			<div class="tabbable" id="tabs-275223" style="margin-top: 110px;">
				<ul class="nav nav-tabs">
					<li>
						<a href="#panel-240406" data-toggle="tab">CLASIFICACI�N</a>
					</li>
					<li class="active">
						<a href="#panel-527779" data-toggle="tab">SINOPSIS</a>
					</li>
				</ul>
				<div class="tab-content">
					<div class="tab-pane" id="panel-240406">
						
							<div class="contenedor2">
							<img class="imagen" src="Imagenes/G.png"/>
							<h2 class="titulo">Para todo p�blico</h2>
							<p class="parrafo">Todas las edades admitidas. No hay desnudos ni referencias sexuales, no hay drogas, alcohol y/o tabaco (estos dos �ltimos si pueden aparecer pero muy poco frecuente), violencia m�nima, muertes naturales y lenguaje cort�s. La mayor�a son pel�culas animadas y la minor�a son pel�culas reales. <br>
							<br>
							Nota: Pueden aparecer armas, 
							pero no se usan para matar y si una se llega a usar para ello no aparece el momento del asesinato.</p>
							</div>
						
					</div>
					<div class="tab-pane active" id="panel-527779">
						<p>
							<div class="contenedor">
<img src="Imagenes/Elnino.png">
 <h1>El Ni�o con la Pijama de Rayas</h1>
		 <p>En el Berl�n de 1942, el peque�o Bruno es hijo de un comandante de un campo de concentraci�n. �l no sabe lo que es el Holocausto, pero lo que s� ha notado es que desde que se mud� a una zona aislada de la ciudad, no tiene a nadie con quien jugar. Un d�a, mientras observa en el campo a esas extra�as personas que parecen llevar un pijama de rayas, conoce a un ni�o jud�o llamado Shmue. 
		 Al mismo tiempo que pierden su inocencia infantil, los dos entablar�n una amistad de consecuencias insospechadas.</p>
		  <h3>Trailer</h3>
		<iframe width="430" height="320" src="http://www.youtube.com/embed/rzow19gyNqQ" frameborder="0" allowfullscreen></iframe>
</div>
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="Button"><a href="Horariodelnino.jsp"><button>Comprar Entradas</button></a></div>
</div>
   <script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
</body>
</html>