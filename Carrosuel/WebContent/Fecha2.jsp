<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Cine Kinal</title>
<link href="dist/css/bootstrap.min.css" rel="stylesheet">
<script src="assets/js/ie-emulation-modes-warning.js"></script>
<link href="carousel.css" rel="stylesheet">
<link type="text/css" rel="stylesheet" href="Fecha2.css"/>
<meta charset="utf-8">
</head>
<body>
<div class="container">
	<div class="row clearfix">
		<div class="col-md-12 column">
			<nav class="navbar navbar-default navbar-fixed-top navbar-inverse" role="navigation">
				<div class="navbar-header">
					<h2 style="color: #FFFFFF;margin-left: 630px;">Cine Kinal</h2>
				</div>
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav">
						<li style="margin-left:-770px;"x;class="active">
							<a href="Principal.jsp"><h2 style="font-size: 14px;color: white;">Regresar a Cartelera</h2></a>
						</li>
					</ul>			
				</div>
			</nav>
		</div>
	</div>
</div>
<div class="container">
	<div class="row clearfix">
		<div class="col-md-12 column">
			 <a href="Complejo.jsp" class="btn btn-lg btn-default" type="button" style="margin-top: 100px;">Visita nuestros complejos</a>
		</div>
	</div>
</div>
<h3 class="complejo1">Cine Kinal - Miraflores</h3>
<div class="pelicula">
<a href="LosJuegosDelHambre.jsp">
<img src="Imagenes/Thehungergames.png"/>
</a>
<div class="container">
	<div class="row clearfix">
		<div class="col-md-12 column">
			 <a href="LosJuegosDelHambre.jsp" class="btn btn-sm" type="button" style="font-size:16px;margin-left:40px;margin-top:-210px;">Los Juegos del Hambre</a>
			 <div class="tipo"><p style="margin-left: 3px;">2D Y 3D</p>></div>
			 
		</div>

		<div class="container">
	<div class="row clearfix">
		<div class="col-md-12 column" style="margin-top: -105px;">
			<p style="width: 250px;">
				El fen�meno mundial de Los juegos del hambre contin�a incendiando el universo con el estreno de Los juegos del hambre: Sinsajo Parte 1.
			</p>
			<p>
				<a class="btn" href="LosJuegosDelHambre.jsp" style="margin-left: -15px;">Leer M�s �</a>
			</p>
		</div>
	</div>
</div>
	</div>
</div>
	<div class="container" style="margin-left: 0px;margin-top: -190px;width: 290px;">
	<div class="row clearfix">
		<div class="col-md-12 column">
			<ul class="breadcrumb">
				<li>
					<a href="#" style="font-size:13px;">10:00 A.M</a> <span class="divider"></span>
				</li>
				<li>
					<a href="#" style="font-size:13px;">02:00 P.M</a> <span class="divider"></span>
				</li>
				<li class="active">
					<a href="#" style="font-size:13px;">08:30 P.M</a> <span class="divider"></span>
				</li>
			</ul>
		</div>
	</div>
</div>
</div>
<div class="pelicula">
<a href="Grandesheroes.jsp">
<img src="Imagenes/Grandeheroes.png"/></a>
<div class="container">
	<div class="row clearfix">
		<div class="col-md-12 column">
			 <a href="Grandesheroes.jsp" class="btn btn-sm" type="button" style="font-size:16px;margin-left:60px;margin-top:-210px;">Grandes Heroes</a>
			 <div class="tipo4"><p style="margin-left: 3px;">PARA TODA LA FAMILIA</p></div>
		</div>
<div class="container">
	<div class="row clearfix">
		<div class="col-md-12 column" style="margin-top: -85px;">
			<p style="width: 250px;">
				Es una comedia de aventuras cargada de accion sobre el prodigio de la robotica Hiro Hamada.
			</p>
			<p>
				<a class="btn" href="Grandesheroes.jsp" style="margin-left: -15px;">Leer M�s �</a>
			</p>
		</div>
	</div>
</div>
	</div>
</div>
<div class="container" style="margin-left: 0px;margin-top: -190px;width: 290px;">
	<div class="row clearfix">
		<div class="col-md-12 column">
			<ul class="breadcrumb">
				<li>
					<a href="#" style="font-size:13px;">10:00 A.M</a> <span class="divider"></span>
				</li>
				<li>
					<a href="#" style="font-size:13px;">02:00 P.M</a> <span class="divider"></span>
				</li>
				<li class="active">
					<a href="#" style="font-size:13px;">08:30 P.M</a> <span class="divider"></span>
				</li>
			</ul>
		</div>
	</div>
</div>
</div>
<h3 class="complejo2">Cine Kinal - Cayal�</h3>
<div class="pelicula">
<img src="Imagenes/Harrypotter.png"/>
<div class="container">
	<div class="row clearfix">
		<div class="col-md-12 column">
			 <a href="Harrypotter.jsp" class="btn btn-sm" type="button" style="font-size:16px;margin-left:20px;margin-top:-210px;">Harry Potter (Reliquias de la muerte)</a>
			 <div class="tipo"><p style="margin-left: 3px;">2D Y 3D</p>></div>
		</div>
		<div class="container">
	<div class="row clearfix">
		<div class="col-md-12 column" style="margin-top: -105px;">
			<p style="width: 250px;">
				Al igual que en la primera parte de esta �ltima entrega cinematogr�fica inspirada en los libros superventas de la autora J.K. Rowling.
			</p>
			<p>
				<a class="btn" href="Harrypotter.jsp" style="margin-left: -15px;">Leer M�s �</a>
			</p>
		</div>
	</div>
</div>
	</div>
</div>
<div class="container" style="margin-left: 0px;margin-top: -190px;width: 290px;">
	<div class="row clearfix">
		<div class="col-md-12 column">
			<ul class="breadcrumb">
				<li>
					<a href="#" style="font-size:13px;">10:00 A.M</a> <span class="divider"></span>
				</li>
				<li>
					<a href="#" style="font-size:13px;">02:00 P.M</a> <span class="divider"></span>
				</li>
				<li class="active">
					<a href="#" style="font-size:13px;">08:30 P.M</a> <span class="divider"></span>
				</li>
			</ul>
		</div>
	</div>
</div>
</div>
<h3 class="complejo3">Cine Kinal - Pradera Xela, Quetzaltenango</h3>
<div class="pelicula">
<a href="Elhombrearana.jsp">
<img src="Imagenes/Elhombrearana.png"/></a>
<div class="container">
	<div class="row clearfix">
		<div class="col-md-12 column">
			 <a href="Elhombrearana.jsp" class="btn btn-sm" type="button" style="font-size:16px;margin-left:70px;margin-top:-210px;">El Hombre Ara�a 3</a>
			 <div class="tipo"><p style="margin-left: 3px;">2D Y 3D</p>></div>
		</div>
			<div class="container">
	<div class="row clearfix">
		<div class="col-md-12 column" style="margin-top: -120px;">
			<p style="width: 250px;">
				Es genial ser el Hombre Ara�a. Para Peter Parker, no hay nada que se le parezca a estar oscilando entre los rascacielos, ser el h�roe y pasar tiempo con Gwen Stacy.
			</p>
			<p>
				<a class="btn" href="Elhombrearana.jsp" style="margin-left: -15px;">Leer M�s �</a>
			</p>
		</div>
	</div>
</div>
	</div>
</div>
<div class="container" style="margin-left: 0px;margin-top: -190px;width: 290px;">
	<div class="row clearfix">
		<div class="col-md-12 column">
			<ul class="breadcrumb">
				<li>
					<a href="#" style="font-size:13px;">10:00 A.M</a> <span class="divider"></span>
				</li>
				<li>
					<a href="#" style="font-size:13px;">02:00 P.M</a> <span class="divider"></span>
				</li>
				<li class="active">
					<a href="#" style="font-size:13px;">08:30 P.M</a> <span class="divider"></span>
				</li>
			</ul>
		</div>
	</div>
</div>
</div>
<h3 class="complejo4">Cine Kinal- Oakland Mall</h3>
<div class="pelicula">
<a href="Cars.jsp">
<img src="Imagenes/Cars2.png"/></a>
<div class="container">
	<div class="row clearfix">
		<div class="col-md-12 column">
			 <a href="Cars.jsp" class="btn btn-sm" type="button" style="font-size:16px;margin-left:80px;margin-top:-210px;">Cars 2</a>
			 <div class="tipo"><p style="margin-left: 3px;">2D Y 3D</p>></div>
		</div>
						<div class="container">
	<div class="row clearfix">
		<div class="col-md-12 column" style="margin-top: -105px;">
			<p style="width: 250px;">
				 Rayo McQueen y su colega Mate participan en el primer Grand Prix Mundial, una emocionante carrera por etapas.
			</p>
			<p>
				<a class="btn" href="Cars.jsp" style="margin-left: -15px;">Leer M�s �</a>
			</p>
		</div>
	</div>
</div>
	</div>
</div>
<div class="container" style="margin-left: 0px;margin-top: -190px;width: 290px;">
	<div class="row clearfix">
		<div class="col-md-12 column">
			<ul class="breadcrumb">
				<li>
					<a href="#" style="font-size:13px;">10:00 A.M</a> <span class="divider"></span>
				</li>
				<li>
					<a href="#" style="font-size:13px;">02:00 P.M</a> <span class="divider"></span>
				</li>
				<li class="active">
					<a href="#" style="font-size:13px;">08:30 P.M</a> <span class="divider"></span>
				</li>
			</ul>
		</div>
	</div>
</div>
</div>
</body>
</html>