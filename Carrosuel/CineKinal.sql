CREATE TABLE pelicula (
  ID            NUMBER(10)    NOT NULL,
  NOMBRE        VARCHAR(300)   NOT NULL,
  DESCRIPCION VARCHAR(300) NOT NULL,
  DURACION VARCHAR(150) NOT NULL,
  ID_CLASIFICACION NUMBER(10) NOT NULL
  );

ALTER TABLE pelicula ADD (
  CONSTRAINT pelicula_pk PRIMARY KEY (ID),
  FOREIGN KEY (ID_CLASIFICACION) REFERENCES clasificacion(ID));

CREATE SEQUENCE pelicula_seq;

CREATE OR REPLACE TRIGGER pelicula_bir 
BEFORE INSERT ON pelicula 
FOR EACH ROW

BEGIN
  SELECT pelicula_seq.NEXTVAL
  INTO   :new.id
  FROM   dual;
END;
INSERT INTO pelicula (NOMBRE,DESCRIPCION,DURACION,IMAGEN,VIDEO,ID_CLASIFICACION) VALUES ('EL HOMBRE ARA�A','INTERESANTE','CUATRO HORAS',41);
INSERT INTO pelicula (NOMBRE,DESCRIPCION,DURACION,IMAGEN,VIDEO,ID_CLASIFICACION) VALUES ('GRANDES HEROES','INTERESANTE','DOS HORAS Y MEDI2A',42);
INSERT INTO pelicula (NOMBRE,DESCRIPCION,DURACION,ID_CLASIFICACION) VALUES ('LOS JUEGOS DEL HAMBRE','INTERESANTE','DOS HORAS Y MEDIA',43);
INSERT INTO pelicula (NOMBRE,DESCRIPCION,DURACION,ID_CLASIFICACION) VALUES ('CARS 2','INTERESANTE','DOS HORAS Y MEDIA',44);
INSERT INTO pelicula (NOMBRE,DESCRIPCION,DURACION,ID_CLASIFICACION) VALUES ('HARRY POTTER','INTERESANTE','DOS HORAS Y MEDIA',45);
INSERT INTO pelicula (NOMBRE,DESCRIPCION,DURACION,ID_CLASIFICACION) VALUES ('EL NI�O DE LA PIJAMA EN RAYAS','INTERESANTE','DOS HORAS Y MEDIA',46);
INSERT INTO pelicula (NOMBRE,DESCRIPCION,DURACION,ID_CLASIFICACION) VALUES ('PRIMICA MORTAL','INTERESANTE','DOS HORAS Y MEDIA',47);
INSERT INTO pelicula (NOMBRE,DESCRIPCION,DURACION,ID_CLASIFICACION) VALUES ('TRANSFORMES','INTERESANTE','DOS HORAS Y MEDIA',48);

SELECT * FROM pelicula;

SELECT PELICULA.NOMBRE,PELICULA.DESCRIPCION,PELICULA.DURACION,PELICULA.IMAGEN,PELICULA.VIDEO , CLASIFICACION.NOMBRE AS CLASIFICACION FROM PELICULA INNER JOIN CLASIFICACION
ON PELICULA.ID_CLASIFICACION = CLASIFICACION.ID;
--------------------------------------------------------------------------------------------------------------------------

CREATE TABLE complejo (
  ID           NUMBER(10)    NOT NULL,
  NOMBRE       VARCHAR(150)   NOT NULL,
  DIRECCION    VARCHAR(500)  NOT NULL,
  CALL_CENTER  NUMBER(10)  NOT NULL,
  LATITUD     DECIMAL(9,6)  NOT NULL,
  LONGITUD     DECIMAL(9,6)  NOT NULL);

ALTER TABLE complejo ADD (
  CONSTRAINT complejo_pk PRIMARY KEY (ID));

CREATE SEQUENCE complejo_seq;

CREATE OR REPLACE TRIGGER complejo_bir 
BEFORE INSERT ON complejo 
FOR EACH ROW

BEGIN
  SELECT complejo_seq.NEXTVAL
  INTO   :new.id
  FROM   dual;
END;


INSERT INTO complejo (NOMBRE,DIRECCION,CALL_CENTER,LATITUD,LONGITUD) VALUES ('KINAL','6A Avenida 13-54',23877600,14.625431,-90.535983);
SELECT * FROM complejo;
--------------------------------------------------------------------------------------------------------------------------
CREATE TABLE sala (
  ID            NUMBER(10)    NOT NULL,
  NOMBRE        VARCHAR(300)   NOT NULL,
  TIPO          VARCHAR(300)   NOT NULL,
  ID_COMPLEJO   NUMBER(10)  NOT NULL);

ALTER TABLE sala ADD (
  CONSTRAINT sala_pk PRIMARY KEY (ID),
  FOREIGN KEY (ID_COMPLEJO) REFERENCES complejo(ID));

CREATE SEQUENCE sala_seq;

CREATE OR REPLACE TRIGGER sala_bir 
BEFORE INSERT ON sala 
FOR EACH ROW

BEGIN
  SELECT sala_seq.NEXTVAL
  INTO   :new.id
  FROM   dual;
END;

INSERT INTO sala (NOMBRE,TIPO,ID_COMPLEJO) VALUES ('Macro XE','M�XIMA TECNOLOG�A',1);
SELECT * FROM sala;
--------------------------------------------------------------------------------------------------------------------------
CREATE TABLE asiento (
  ID           NUMBER(10)    NOT NULL,
  NOMBRE       VARCHAR(100)   NOT NULL,
  ID_SALA       NUMBER(10)   NOT NULL
  );

ALTER TABLE asiento ADD (
  CONSTRAINT asiento_pk PRIMARY KEY (ID),
  FOREIGN KEY (ID_SALA) REFERENCES sala(ID));

CREATE SEQUENCE asiento_seq;

CREATE OR REPLACE TRIGGER asiento_bir 
BEFORE INSERT ON asiento
FOR EACH ROW

BEGIN
  SELECT asiento_seq.NEXTVAL
  INTO   :new.id
  FROM   dual;
END;

INSERT INTO asiento (NOMBRE,ID_SALA) VALUES ('VIP',1);
SELECT * FROM asiento;
--------------------------------------------------------------------------------------------------------------------------
CREATE TABLE cartelera (
  ID           NUMBER(10)    NOT NULL,
  HORAINICIO   DECIMAL(9,6) NOT NULL,
  HORAFINAL   DECIMAL(9,6) NOT NULL,
  FECHA VARCHAR(100) NOT NULL,
  ID_SALA     NUMBER(10) NOT NULL,
  ID_PELICULA NUMBER(10) NOT NULL
  );

ALTER TABLE cartelera ADD (
  CONSTRAINT cartelera_pk PRIMARY KEY (ID),
  FOREIGN KEY (ID_SALA) REFERENCES sala(ID),
  FOREIGN KEY (ID_PELICULA) REFERENCES pelicula(ID));

CREATE SEQUENCE cartelera_seq;

CREATE OR REPLACE TRIGGER cartelera_bir 
BEFORE INSERT ON cartelera
FOR EACH ROW

BEGIN
  SELECT cartelera_seq.NEXTVAL
  INTO   :new.id
  FROM   dual;
END;

INSERT INTO cartelera (HORAINICIO,HORAFINAL,FECHA,ID_SALA,ID_PELICULA) VALUES (5.30,7.35,'Marzo',1,1);
INSERT INTO cartelera (HORAINICIO,HORAFINAL,FECHA,ID_SALA,ID_PELICULA) VALUES (5.58,5.30,'ENERO',1,1);

--------------------------------------------------------------------------------------------------------------------------
CREATE TABLE categoria (
  ID           NUMBER(10)    NOT NULL,
  NOMBRE      VARCHAR(100) NOT NULL,
  DESCRIPCION VARCHAR(100) NOT NULL
  );

ALTER TABLE categoria ADD (
  CONSTRAINT categoria_pk PRIMARY KEY (ID));
 

CREATE SEQUENCE categoria_seq;

CREATE OR REPLACE TRIGGER categoria_bir 
BEFORE INSERT ON categoria
FOR EACH ROW

BEGIN
  SELECT categoria_seq.NEXTVAL
  INTO   :new.id
  FROM   dual;
END;

INSERT INTO categoria (NOMBRE,DESCRIPCION) VALUES ('DE MIEDO','ESTARA GENIAL');
SELECT * FROM categoria;
--------------------------------------------------------------------------------------------------------------------------
CREATE TABLE clasificacion (
  ID           NUMBER(10)    NOT NULL,
  NOMBRE       VARCHAR(100)   NOT NULL,
  DESCRIPCION   VARCHAR(100)  NOT NULL
  );

ALTER TABLE clasificacion ADD (
  CONSTRAINT clasificacion_pk PRIMARY KEY (ID));

CREATE SEQUENCE clasificacion_seq;

CREATE OR REPLACE TRIGGER clasificacion_bir 
BEFORE INSERT ON clasificacion 
FOR EACH ROW

BEGIN
  SELECT clasificacion_seq.NEXTVAL
  INTO   :new.id
  FROM   dual;
END;

INSERT INTO clasificacion (NOMBRE, DESCRIPCION) VALUES ('B-15','PARA MAYORES DE 15 A�OS');
INSERT INTO clasificacion (NOMBRE, DESCRIPCION) VALUES ('B','PARA TODA LA FAMILIA');
INSERT INTO clasificacion (NOMBRE, DESCRIPCION) VALUES ('C','MAYORES DE EDAD');
INSERT INTO clasificacion (NOMBRE, DESCRIPCION) VALUES ('A','PARA TODO PUBLICO');
INSERT INTO clasificacion (NOMBRE, DESCRIPCION) VALUES ('C','MAYORES DE EDAD');
INSERT INTO clasificacion (NOMBRE, DESCRIPCION) VALUES ('B','MAYORES DE 12 A�OS');
INSERT INTO clasificacion (NOMBRE, DESCRIPCION) VALUES ('B-15','PARA MAYORESD DE 15 A�OS');
INSERT INTO clasificacion (NOMBRE, DESCRIPCION) VALUES ('B','MAYORES DE 12 A�OS');
SELECT * FROM clasificacion;
--------------------------------------------------------------------------------------------------------------------------
CREATE TABLE estreno (
  ID           NUMBER(10)    NOT NULL,
 FECHA  VARCHAR(100) NOT NULL,
 ID_PELICULA NUMBER(10) NOT NULL
  );

ALTER TABLE estreno ADD (
  CONSTRAINT estreno_pk PRIMARY KEY (ID),
  FOREIGN KEY (ID_PELICULA) REFERENCES pelicula(ID)
  );
  
  CREATE SEQUENCE estreno_seq;


CREATE OR REPLACE TRIGGER estreno_bir 
BEFORE INSERT ON estreno
FOR EACH ROW

BEGIN
  SELECT estreno_seq.NEXTVAL
  INTO   :new.id
  FROM   dual;
END;
INSERT INTO estreno (FECHA,ID_PELICULA) VALUES('MARZO',1);
INSERT INTO estreno (FECHA,ID_PELICULA) VALUES('FEBRER',2);
--------------------------------------------------------------------------------------------------------------------------
CREATE TABLE pelicula_categoria (
  ID           NUMBER(10)    NOT NULL,
  ID_PELICULA NUMBER(10) NOT NULL,
  ID_CATEGORIA NUMBER(10) NOT NULL
);

ALTER TABLE pelicula_categoria ADD (
  CONSTRAINT pelicula_categoria_pk PRIMARY KEY (ID),
  FOREIGN KEY (ID_PELICULA) REFERENCES pelicula(ID),
  FOREIGN KEY (ID_CATEGORIA) REFERENCES categoria(ID)
);
  
CREATE SEQUENCE pelicula_categoria_seq;


CREATE OR REPLACE TRIGGER pelicula_categoria_bir 
BEFORE INSERT ON pelicula_categoria
FOR EACH ROW

BEGIN
  SELECT pelicula_categoria_seq.NEXTVAL
  INTO   :new.id
  FROM   dual;
END;
INSERT INTO pelicula_categoria (ID_PELICULA,ID_CATEGORIA) VALUES (2,1);
--------------------------------------------------------------------------------------------------------------------------
CREATE TABLE funcion (
  ID           NUMBER(10)    NOT NULL,
 NOMBRE VARCHAR(100) NOT NULL,
 DESCRIPCION VARCHAR(100) NOT NULL,
 PRECIO VARCHAR(100) NOT NULL
  );

ALTER TABLE funcion ADD (
  CONSTRAINT funcion_pk PRIMARY KEY (ID)
  );
  
  CREATE SEQUENCE funcion_seq;


CREATE OR REPLACE TRIGGER funcion_bir 
BEFORE INSERT ON funcion
FOR EACH ROW

BEGIN
  SELECT funcion_seq.NEXTVAL
  INTO   :new.id
  FROM   dual;
END;

INSERT INTO funcion (NOMBRE,DESCRIPCION,PRECIO) VALUES ('Los Juegos del Hambre','Interesante','Q.25');
--------------------------------------------------------------------------------------------------------------------------

CREATE TABLE venta (
  ID           NUMBER(10)    NOT NULL,
 NOMBREPERSONA VARCHAR(100) NOT NULL,
 NOTRANSAGCION NUMBER(10) NOT NULL,
 CORREO VARCHAR(100) NOT NULL,
 TELEFONO NUMBER(10) NOT NULL,
 TIPO VARCHAR(100) NOT NULL
  );

ALTER TABLE venta ADD (
  CONSTRAINT venta_pk PRIMARY KEY (ID)
  );
  
  CREATE SEQUENCE venta_seq;


CREATE OR REPLACE TRIGGER venta_bir 
BEFORE INSERT ON venta
FOR EACH ROW

BEGIN
  SELECT venta_seq.NEXTVAL
  INTO   :new.id
  FROM   dual;
END;
INSERT INTO venta (NOMBREPERSONA,NOTRANSAGCION,CORREO,TELEFONO,TIPO) VALUES ('Jose Daniel Samayoa',5896484,'jose_samayo@hotmail.com',58964752,'VIP');
--------------------------------------------------------------------------------------------------------------------------
CREATE TABLE venta_detalle (
  ID           NUMBER(10)    NOT NULL,
  ID_VENTA NUMBER(10) NOT NULL,
  ID_ASIENTO NUMBER(10) NOT NULL,
  ID_CARTELERA NUMBER(10) NOT NULL,
  ID_FUNCION NUMBER(10) NOT NULL
  );

ALTER TABLE venta_detalle ADD (
  CONSTRAINT venta_detalle_pk PRIMARY KEY (ID),
  FOREIGN KEY (ID_VENTA) REFERENCES venta(ID),
  FOREIGN KEY (ID_ASIENTO) REFERENCES asiento(ID),
  FOREIGN KEY (ID_CARTELERA) REFERENCES cartelera(ID),
  FOREIGN KEY (ID_FUNCION) REFERENCES funcion(ID)
  );
  
  CREATE SEQUENCE venta_detalle_seq;


CREATE OR REPLACE TRIGGER venta_detalle_bir 
BEFORE INSERT ON venta_detalle
FOR EACH ROW

BEGIN
  SELECT venta_detalle_seq.NEXTVAL
  INTO   :new.id
  FROM   dual;
END;
INSERT INTO venta_detalle (ID_VENTA,ID_ASIENTO,ID_CARTELERA,ID_FUNCION) VALUES (1,1,1,1);
SELECT * FROM venta_detalle;

